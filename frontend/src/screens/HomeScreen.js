import {Grid,Heading} from '@chakra-ui/react'
import { useEffect } from 'react'
import { useDispatch,useSelector } from 'react-redux'

import Loader from '../components/Loader'
import { listProducts } from '../actions/productActions' 
import ProductCard from '../components/ProductCard'
import Message from '../components/Message'

const HomeScreen=() =>{
    const dispatch = useDispatch()

    const productList = useSelector((state)=> state.productList)
    const {loading,error,products} = productList

    useEffect(()=>{
        dispatch(listProducts())

    },[dispatch])
  
    return (
		<>
			<Heading as="h2" mb={{ base: '4', md: '8' }} fontSize={{ base: 'xl', md: '2xl' }}>
        Latest Products
      </Heading>

			{loading ? (
				<Loader/>
			) : error ? (
				<Message type='error'>{error}</Message>
			) : (
				<Grid templateColumns={{ base: '1fr', md: 'repeat(2, 1fr)', xl: 'repeat(4, 1fr)' }} gap={{ base: '4', md: '8' }}>
        {products.map((product) => (
          <ProductCard product={product} key={product._id} />
        ))}
      </Grid>
			)}
		</>
	);
};

export default HomeScreen;